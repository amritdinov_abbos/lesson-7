const timer = document.querySelector(".timer");
const startBtn = document.querySelector(".btn-start");
const stopBtn = document.querySelector(".btn-pause");
const resetBtn = document.querySelector(".btn-reset");

let isActive = false;
let isFirstRender = true;
let interval;

let m = localStorage.getItem("minutes") || 0;
let s = localStorage.getItem("seconds") || 0;

const start = () => {
  interval = setInterval(() => AddTime(), 1000);
};

const AddTime = () => {
  if (s < 59) {
    s++;
  }else{
    m++;
    s = 0;
  }
  refreshTime();
};

const refreshTime = () => {
  const result = `${m < 10 ? "0" + m : m}:${s < 10 ? "0" + s : s}`;
  localStorage.setItem('minutes',m)
  localStorage.setItem('seconds',s)
  timer.innerText = result;
};

startBtn.addEventListener("click", (e) => {
  if (!isActive) {
    start();
    isActive = true;
  }
});
resetBtn.addEventListener("click", (e) => {
  m = 0;
  s = 0;
  clearInterval(interval);
  localStorage.clear();
  isActive = false;
  refreshTime();
});
stopBtn.addEventListener("click", (e) =>{
  clearInterval(interval)
  isActive = false
});

refreshTime();

